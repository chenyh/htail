package com.uebox.http.common;

/**
 * User: chenyh
 * Date: 12-12-29
 * Time: 下午5:30
 */

/**
 * 用于执行HTTP任务,
 */
public interface HttpTask {

    public static final long DEFAULT_BUFFER_SIZE = 512 * 1000L;

    /**
     * 获取内存的长度
     *
     * @return
     */
    public long getContentLength();

    /**
     * 获取内容的类型
     *
     * @return
     */
    public String getContentType();

    /**
     * 是否支持<code>AcceptRanges</code>
     *
     * @return
     */
    public boolean isAcceptRanges();

    /**
     * 是否支持gzip压缩方式
     *
     * @return
     */
    public boolean isGZip();

    /**
     * 执行HTTP任务
     */
    public void doTask() throws Exception;
}
