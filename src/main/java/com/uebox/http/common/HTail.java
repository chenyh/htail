package com.uebox.http.common;

import java.util.concurrent.TimeUnit;

/**
 * User: chenyh
 * Date: 12-12-27
 * Time: 下午10:54
 */
public class HTail {

    /**
     * 默认的字符集
     */
    private static final String DEFAULT_CHARSET = "GBK";

    private static final long DEFAULT_BUFFER_LENGTH = 4 * 1024L;

    /**
     * 主机
     */
    private String host;

    /**
     * url
     */
    private String url;

    /**
     * 内容长度
     */
    private long contentLength;

    /**
     * 当前内容的偏移量
     */
    private long currentOffset;

    /**
     * 所使用的字符集
     */

    private String charset;


    public HTail(String url) {
        this(null, url);
    }

    public HTail(String host, String url) {
        this(host, url, DEFAULT_CHARSET);
    }

    public HTail(String host, String url, String charset) {

        this.host = host;
        this.url = url;
        this.charset = charset;

        if (charset == null || charset.trim().length() < 1) {
            this.charset = DEFAULT_CHARSET;
        }


    }


    /**
     * 执行tail命令
     */
    public void doTail() {
        try {

            HeadHttpTask headHttpTask = new HeadHttpTask(host, url);
            headHttpTask.doTask();
            contentLength = headHttpTask.getContentLength();

            if (contentLength > DEFAULT_BUFFER_LENGTH + 1L) {
                currentOffset = contentLength - DEFAULT_BUFFER_LENGTH - 1;
            }


            while (true) {

                GetHttpTask getHttpTask = new GetHttpTask(host, url, currentOffset, currentOffset + DEFAULT_BUFFER_LENGTH);
                getHttpTask.doTask();

                currentOffset = getHttpTask.getEndPoint();
                contentLength = getHttpTask.getContentLength();
                if(getHttpTask.getContentBytes()!=null&&getHttpTask.getContentBytes().length>0){
                    System.out.print(new String(getHttpTask.getContentBytes(), charset));
                }

                if (contentLength - currentOffset < DEFAULT_BUFFER_LENGTH) {
                    TimeUnit.SECONDS.sleep(2L);
                }
            }

        } catch (Exception exception) {
            System.err.println(exception.getMessage());
        }

    }

    /**
     * 打印命令行帮助
     */
    public static void printHelp() {
        System.out.println("用法:htail [选项]... <url>" + "\r\n" +
                "选项:" + "\r\n" +
                "-h,     host全称,可选参数" + "\r\n" +
                "-c,     用于内容编码的字符集,如果不指定,默认为GBK编码" + "\r\n" +
                "url,    参数为必选项");
        System.exit(0);
    }


    public static void main(String[] args) {
        if (args == null || args.length < 1) {
            System.out.println("参数错误:");
            printHelp();
        }

        String host = null;
        String url = null;
        String charset = null;

        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-h")) {
                host = args[i + 1];
                i++;
                continue;
            } else if (args[i].equals("-c")) {
                charset = args[i + 1];
                i++;
                continue;
            } else {
                if (args[i] == null) {
                    printHelp();
                }

                url = args[i];
            }
        }

        HTail tail = new HTail(host, url, charset);
        tail.doTail();
    }

}
