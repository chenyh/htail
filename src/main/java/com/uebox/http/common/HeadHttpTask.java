package com.uebox.http.common;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * User: chenyh
 * Date: 12-12-29
 * Time: 下午5:19
 */
public class HeadHttpTask implements HttpTask {
    private String host;
    private String url;


    private long contentLength;

    private String contentType;

    private boolean acceptRanges;


    public HeadHttpTask(String host, String url) {
        this.host = host;
        this.url = url;
    }


    @Override
    public void doTask() throws Exception {
        HttpURLConnection connection;

        URL url = new URL(this.url);
        connection = HttpConnectionUtil.createDefaultConnection(url);

        connection.setRequestMethod("HEAD");

        if (host != null && host.trim().length() > 0)
            connection.addRequestProperty("Host", host);

        if (connection.getResponseCode() != 200) {
            throw new TaskException("http status:" + Integer.toString(connection.getResponseCode()));
        }

        this.setContentLength(connection.getContentLength());
        this.setContentType(connection.getContentType());
        if(connection.getHeaderField("Accept-Ranges")!=null&&connection.getHeaderField("Accept-Ranges").equals("bytes")){
            this.setAcceptRanges(true);
        }

        if(connection!=null)
            connection.disconnect();

    }

    @Override
    public boolean isGZip() {
        return false;
    }

    @Override
    public long getContentLength() {
        return contentLength;
    }

    @Override
    public String getContentType() {
        return contentType;
    }

    @Override
    public boolean isAcceptRanges() {
        return acceptRanges;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setAcceptRanges(boolean acceptRanges) {
        this.acceptRanges = acceptRanges;
    }
}
