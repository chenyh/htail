package com.uebox.http.common;

/**
 * User: chenyh
 * Date: 13-1-2
 * Time: 下午4:29
 */
public class TaskException extends RuntimeException {
    private String message;

    private Exception cause;

    public TaskException(String message){

        this(message,null);
    }

    public TaskException(String message, Exception cause){
        super(message,cause);
        this.message = message;
        this.cause = cause;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Exception getCause() {
        return cause;
    }

    public void setCause(Exception cause) {
        this.cause = cause;
    }
}
