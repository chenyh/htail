package com.uebox.http.common;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * User: chenyh
 * Date: 12-12-29
 * Time: 下午5:20
 */
public class GetHttpTask implements HttpTask {

    private String host;
    private String url;


    private long contentLength;

    private String contentType;

    private boolean acceptRanges;

    private byte[] contentBytes;

    private long startPoint;

    private long endPoint;


    public GetHttpTask(String host, String url, long startPoint, long endPoint) {
        this.host = host;
        this.url = url;
        this.startPoint = startPoint;
        this.endPoint = endPoint;
    }


    @Override
    public void doTask() throws Exception {
        HttpURLConnection connection;

        URL url = new URL(this.url);
        connection = HttpConnectionUtil.createDefaultConnection(url);
        connection.setRequestMethod("GET");
        connection.addRequestProperty("Range", "bytes=" + startPoint + '-' + endPoint);

        if (host != null && host.trim().length() > 0)
            connection.addRequestProperty("Host", host);

        if (connection.getResponseCode() < 200 || connection.getResponseCode() > 300) {
            throw new TaskException("http status:" + Integer.toString(connection.getResponseCode()) + ' ' + connection.getResponseMessage());
        }

        //this.setContentLength(connection.getContentLengthLong());
        this.setContentType(connection.getContentType());
        if (connection.getHeaderField("Accept-Ranges") != null && connection.getHeaderField("Accept-Ranges").trim().equals("bytes")) {
            this.setAcceptRanges(true);
        }

        String contentRange = connection.getHeaderField("Content-Range");
        if (contentRange != null && contentRange.trim().length() > 0) {
            contentRange = contentRange.trim();
            String temp = contentRange.substring(6);
            String[] total = temp.split("/");
            String[] point = total[0].split("-");

            this.startPoint = Long.parseLong(point[0]);
            this.endPoint = Long.parseLong(point[1]);
            this.contentLength = Long.parseLong(total[1]);
        }
        this.contentType = connection.getHeaderField("Content-Type");
        this.contentBytes = converToByteArray(connection.getInputStream());

        if(connection.getContentLength()==1&&contentBytes[0]==(byte)0){
            this.contentBytes = null;
        }

        if(connection!=null)
            connection.disconnect();
    }

    private byte[] converToByteArray(InputStream inputStream) {

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int read;
        byte[] data = new byte[2046];

        try {
            while ((read = inputStream.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, read);
            }
            buffer.flush();
        } catch (IOException e) {

        }

        return buffer.toByteArray();
    }

    @Override
    public boolean isGZip() {
        return false;
    }

    @Override
    public long getContentLength() {
        return contentLength;
    }

    @Override
    public String getContentType() {
        return contentType;
    }

    @Override
    public boolean isAcceptRanges() {
        return acceptRanges;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setAcceptRanges(boolean acceptRanges) {
        this.acceptRanges = acceptRanges;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public byte[] getContentBytes() {
        return contentBytes;
    }

    public void setContentBytes(byte[] contentBytes) {
        this.contentBytes = contentBytes;
    }

    public long getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(long startPoint) {
        this.startPoint = startPoint;
    }

    public long getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(long endPoint) {
        this.endPoint = endPoint;
    }
}
