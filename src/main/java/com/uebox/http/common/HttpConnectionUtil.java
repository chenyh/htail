package com.uebox.http.common;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * User: chenyh
 * Date: 12-12-29
 * Time: 下午6:08
 */
public class HttpConnectionUtil {
    public static HttpURLConnection createDefaultConnection(URL url) {
        try {
            HttpURLConnection connection;
            connection = (HttpURLConnection) url.openConnection();
            connection.setInstanceFollowRedirects(false);
            connection.setDefaultUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            return connection;
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }
}

